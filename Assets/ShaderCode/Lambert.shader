Shader "Custom/Lambert"
{
    Properties
    {
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Color("Emissive Color", Color) = (1,1,1,1)
        _MySlider("Color Slider", Range(0,10)) = 2.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf BasicDiffuse 

        #pragma target 3.0

        sampler2D _MainTex;
        float4 _Color;
        float _MySlider;

        struct Input
        {
            float2 uv_MainTex;
        };
        
        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            
            o.Alpha = c.a;
        }

        //Lambert Light Model
        inline float4 LightingBasicDiffuse(SurfaceOutput s, fixed3 lightDir, fixed atten)
        {
            float difLight = max(0, dot(s.Normal, lightDir));
            float4 col;
            col.rgb = s.Albedo * _LightColor0.rgb * (difLight * atten * 2);
            col.a = s.Alpha;
            return col;
        }


   
        ENDCG
    }
    FallBack "Diffuse"
}
