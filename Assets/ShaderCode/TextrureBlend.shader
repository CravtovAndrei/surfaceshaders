Shader "Custom/TextrureBlend"
{
    Properties
    {
        _MainTint("Main Tint", Color) = (1,1,1,1)
        
        _ColorA("Color A", Color) = (1,1,1,1)
        _ColorB("Color B", Color) = (1,1,1,1)
        
        _RTexture("Red Channel Texture", 2D) = "" {}
        _GTexture("Green Channel Texture", 2D) = "" {}
        _BTexture("Blue Channel Texture", 2D) = "" {}
        _ATexture("Alpha Channel Texture", 2D) = "" {}
        _BlendTexture("Blend Texture", 2D) = "" {}
        
        _NormalTexture("Normal Texture", 2D) = "bump" {}
        
        _RBlendAmount("RBlendAmount", Range(0,1)) = 0
        _GBlendAmount("GBlendAmount", Range(0,1)) = 0
        _BBlendAmount("BBlendAmount", Range(0,1)) = 0
        _ABlendAmount("ABlendAmount", Range(0,1)) = 0
        
        _NormalIntensity("Normal Intensity", Range(0,10)) = 1
        
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.5

        float4 _MainTint;
        float4 _ColorA;
        float4 _ColorB;

        sampler2D _RTexture;
        sampler2D _GTexture;
        sampler2D _BTexture;
        sampler2D _ATexture;
        sampler2D _BlendTexture;
        sampler2D _NormalTexture;


        float _RBlendAmount;
        float _GBlendAmount;
        float _BBlendAmount;
        float _ABlendAmount;
        float _NormalIntensity;

        struct Input
        {
            float2 uv_RTexture;
            float2 uv_GTexture;
            float2 uv_BTexture;
            float2 uv_ATexture;
            float2 uv_BlendTexture;
            float2 uv_NormalTexture;
        };


        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 blendTexData = tex2D (_BlendTexture, IN.uv_BlendTexture);
            float3 normalMap = UnpackNormal(tex2D(_NormalTexture, IN.uv_NormalTexture));
            normalMap = float3(normalMap.x * _NormalIntensity, normalMap.y * _NormalIntensity, normalMap.z);
            
            fixed4 rTexData = tex2D (_RTexture, IN.uv_RTexture);
            fixed4 gTexData = tex2D (_GTexture, IN.uv_GTexture);
            fixed4 bTexData = tex2D (_BTexture, IN.uv_BTexture);
            fixed4 aTexData = tex2D (_ATexture, IN.uv_ATexture);

            float4 finalColor;

            finalColor = lerp(rTexData, gTexData, blendTexData.g * _RBlendAmount );
            finalColor = lerp(finalColor, bTexData, blendTexData.b * _GBlendAmount);
            finalColor = lerp(finalColor, aTexData, blendTexData.a * _BBlendAmount);
            finalColor.a = 1.0;

            float4 terrainLayers = lerp(_ColorA, _ColorB, blendTexData.r * _ABlendAmount);
            finalColor *= terrainLayers;
            finalColor = saturate(finalColor);

            o.Albedo = finalColor.rgb * _MainTint.rgb;
            
            o.Normal = normalMap.rgb;
            
            o.Alpha = finalColor.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
