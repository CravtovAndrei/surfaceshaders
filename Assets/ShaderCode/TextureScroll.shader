Shader "Custom/Texture Scroll"
{
    Properties
    {
        _MainTex("Main(RGB)", 2D) = "white" {}
        _BRDFTexture("BRDF(RGB)", 2D) = "white" {}

        _Color("Emissive Color", Color) = (1,1,1,1)
        _AmbientColor("Ambient Color", Color) = (1,1,1,1)
        _MySlider("Color Slider", Range(0,10)) = 2.5

        _ScrollX("X Scroll", Range(0,2)) = 1 
        _ScrollY("Y Scroll", Range(0,2)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf BasicDiffuse 

        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _BRDFTexture;

        float4 _Color;
        float4 _AmbientColor;
        float _MySlider;
        
        float _ScrollX;
        float _ScrollY;

        struct Input
        {
            float2 uv_MainTex;
        };
        
        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 c = _Color;
            o.Albedo = c.rgb;

			float2 scrolledUV = IN.uv_MainTex;
			fixed xScrollValue = _ScrollX * _Time.x;
			fixed yScrollValue = _ScrollY * _Time.x;

			scrolledUV += float2(xScrollValue,yScrollValue);

			float4 color = tex2D(_MainTex, scrolledUV);

			o.Albedo = color.rgb;
            
            o.Alpha = c.a;
        }


        //Half-Lambert Light Model
        
        inline float4 LightingBasicDiffuse(SurfaceOutput s, fixed3 lightDir, half3 viewDir, fixed atten)
        {
            float difLight = dot(s.Normal, lightDir);
            float rimLight = dot(s.Normal, viewDir);
            float hLambert = difLight * 0.5 + 0.5;
            
           float3 ramp = tex2D (_BRDFTexture, float2(hLambert, rimLight)).rgb;
            float4 col;
            col.rgb = s.Albedo * _LightColor0.rgb * (ramp);
            col.a = s.Alpha;
            return col;
        }
   
        ENDCG
    }
    FallBack "Diffuse"
}
